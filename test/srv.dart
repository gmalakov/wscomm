//import 'dart:async';
import 'dart:async';

import 'package:wscomm/server.dart';

Future main() async {
  SocketHandler.handler = (path, pData, callBack) {
    print('Path:$path');
    print('pData len:${pData.keys}');

    if (callBack != null)
      callBack(pData);
  };

  final wss = WebSocketServer('localhost', 8888, split: true, mtu: 1572864);
  final sock = new TcpSocketServer('localhost', 8889, split: true, mtu: 1572864);

  final srvW = new SocketHandler<WebSocketLibrary, WebSocketServer>(wss);
  final srvT = new SocketHandler<TcpSocketLibrary, TcpSocketServer>(sock);

  sock.onConnect = ((id) async {
    print('SOCK: $id Connected');
    dynamic res = await srvT.sendMessage(id, 'srvmsg', [1,2,3,4,5,6,7,8,9,10]);
    print('SOCK srv call: $res');

    Function err = sock.instances[id].onError;
    sock.instances[id].onError = (() {
      if (err != null)  err();
      print('SOCK: $id Disconnected!');
    });
  });

  wss.onConnect = ((id) async {
    print('WS: $id Connected');
    dynamic res = await srvW.sendMessage(id, 'srvmsg', [1,2,3,4,5,6,7,8,9,10]);
    print('WS srv call: $res');

    Function err = wss.instances[id].onError;
    wss.instances[id].onError = (() {
      if (err != null)  err();
      print('WS: $id Disconnected!');
    });
  });



  /*
    typedef void CallBackFunc(dynamic res);
    typedef void HandFunc(dynamic data, CallBackFunc callBack);
    typedef void SHandler(String path, Map pData, CallBackFunc callBack);   */

}
