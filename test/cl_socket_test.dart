@TestOn('browser')

import 'package:wscomm/client.dart';
import 'package:test/test.dart';

List<int>  bigPacket() {
  List<int> data = [];
  for (int i = 0; i < 23444; i++) data.add(i & 0xff);
  return data;
}


void main() {
  List<int> data = bigPacket();

  WSClient.handler = (path, pData, callBack) {
    print('Path:$path');
    print('pData len:${pData.keys}');

    if (callBack != null)
      callBack(pData);
  };

  test('Send packets to WebSocket', () async {

    int port = 8888;
    final path = 'ws://localhost:$port/ws';

    WSClient wsC = new WSClient(path);

    print((await wsC.sendMessage([1,2,3,4,5,6,7,8,9,10], 'test1') as Map).keys);
    print((await wsC.sendMessage(data, 'test2') as Map).keys);
  });

}