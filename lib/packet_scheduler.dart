library packet_scheduler;

import 'dart:async';
import 'shared.dart';

part 'packet_scheduler/socket_library.dart';
part 'packet_scheduler/packet_scheduler.dart';
part 'packet_scheduler/packet_router.dart';
part 'packet_scheduler/socket_client.dart';

typedef SimpleCallBackFunc();


