//@TestOn('browser')
import 'package:wscomm/server.dart';
import 'package:test/test.dart';

List<int>  bigPacket() {
  List<int> data = [];
  for (int i = 0; i < 150000; i++) data.add(i & 0xff);
  return data;
}


void main() {
  List<int> data = bigPacket();
  final host = 'localhost';

  WSClient.handler = (path, pData, callBack) {
    print('Path:$path');
    print('pData len:${pData.keys}');

    if (callBack != null)
      callBack(pData);
  };


  test('Send packets to WebSocket', () async {
    int port = 8888;
    WSClient wsc = new WSClient(host, port);

    print((await wsc.sendMessage([1,2,3,4,5,6,7,8,9,10], 'test1') as Map).keys);
    print((await wsc.sendMessage(data, 'test2') as Map).keys);
  });

  test('Send packets to TcpSocketLibrary', () async {
    int port = 8889;
    TCPClient tcl = new TCPClient(host, port);

    print((await tcl.sendMessage([1,2,3,4,5,6,7,8,9,10], 'test1') as Map).keys);
    print((await tcl.sendMessage(data, 'test2') as Map).keys);
  });


}