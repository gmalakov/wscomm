part of socket_server;

class WebSocketLibrary extends SocketLibrary {
  WebSocket _ws;

  bool get ready => (_ws?.readyState == WebSocket.open); //is websocket ready

  WebSocketLibrary() : super();

  //Close websocket
  close() => _ws.close();

  String _ipAddr;
  String get ipAddr => _ipAddr;

  //Add actual websocket
  init(WebSocket ws,[String ip]) {
    if (onConnect != null) onConnect(); //It's ready in all cases

    _ws = ws; _ipAddr = ip; //Assign websocket and address
    sendStream.listen((data) {
      //print('S:$data');
      if (ready)
        _ws.add(data);
      else if (onError != null) onError(); //Websocket is not connected
    }); //Add received data to websocket

    _ws.listen((data) {
      List<int> tmp = (data as List).cast<int>();
      //print('R:$tmp');
      addReceived(tmp);
    }).onError((e, s) {
      logError('Error $e \n $s');
      if (onError != null) onError();
    }); //Send websocketdata to stream
  }
}

class TcpSocketLibrary extends SocketLibrary {
  Socket _sock;
  SecureSocket _ssock;

  String _host;
  int _port;
  bool _useSecure = false;

  bool get ssl => _useSecure;

  TcpSocketLibrary() : super() {
    sendStream.listen((bytes) {
      if (ssl && _ssock == null)
        return false; //Secure socket is not initialized
      if (!ssl && _sock == null) return false; //Socket is not initialized

      if (ssl)
        _ssock.add(bytes);
      else
        _sock.add(bytes);
    });
  }

  String get ipAddr {
    if (ssl) return _ssock.address.address;
     else return _sock.address.address;
  }

  //Listen to unsecure socket
  void set sock(Socket sock) {
    _useSecure = false;
    _sock = sock;
    listen(); //Start listening to socket
  }

  //Listen to Secure socket
  void set ssock(SecureSocket sock) {
    _useSecure = true;
    _ssock = sock;
    listen(); //Start listening to socket
  }

  Future<bool> init(String host, int port, {bool useSecure: false}) async {
    _host = host;
    _port = port;
    _useSecure = useSecure;

    if (ssl)
      _ssock = await SecureSocket.connect(_host, _port,
          onBadCertificate: (X509Certificate c) {
        return true;
      }).catchError((err) {
        logError("SecureSocket connect:" + err.toString());
        if (onError != null) onError(); //Socket not connected
      });
    else
      _sock = await Socket.connect(_host, _port).catchError((err) {
        logError("SecureSocket connect:" + err.toString());
        if (onError != null) onError(); //Socket not connected
      });

    if (ssl && _ssock == null) return false;
    if (!ssl && _sock == null)  return false;

    if (ssl)
      _ssock.handleError((dynamic err) {
        logError("SecureSocket:" + err.toString());
        init(host, port, useSecure: useSecure);
        if (onError != null) onError(); //Socket not connected
      });
    else
      _sock.handleError((dynamic err) {
        logError("Socket:" + err.toString());
        init(host, port, useSecure: useSecure);
        if (onError != null) onError(); //Socket not connected
      });

    //Initialize stream
    listen();
    if (ssl)
      return (_ssock != null);
    else
      return (_sock != null);
  }

  void listen() {
    //We are connected here
    if (onConnect != null) onConnect();

    if (ssl) {
      if (_ssock != null)
        _ssock.listen((List<int> data) => super.addReceived(data))..onError((e) {
          logError('Error: $e');
          //Execute on error function if not null
          if (onError != null) onError();
        });
    } else {
      if (_sock != null)
        _sock.listen((List<int> data) => super.addReceived(data))..onError((e) {
          logError('Error: $e');
          //Execute on error function if not null
          if (onError != null) onError();
        });
    }
  }

  void close() {
    super.close(); //Destroy stream
    if (ssl)
      _ssock?.destroy();
    else
      _sock?.destroy();
  }
}




