part of socket_server;

typedef void OnConnect(int id);

abstract class GenSocketServer<S extends SocketLibrary> {
  static int counter = 0;
  String _host;
  int _port;

  bool _useSecure;
  bool get ssl => _useSecure;
  bool _split;
  int _mtu;

  OnConnect onConnect;
  Map<int, S> instances = {};
  List<int>  get instanceIds => instances.keys;

  Map<int, PacketRouter<S>> schedulers = {};

  GenSocketServer(this._host, this._port, { bool ssl = false,
    bool split = true, int mtu = PacketScheduler.baseMtu }) {
    _split = split;
    _mtu = mtu;
    _useSecure = ssl;
  }

  PacketRouter<S> createObject(S sock) =>
      new PacketRouter(sock,split: _split, mtu: _mtu);

  removeInstance(int id) {
    S instance = instances[id];
    instance?.close();

    instances.remove(id);
  }

  //Sending data to scheduler controller
  void send(int id, List<int> bytes) => schedulers[id].send(bytes);

  //Receive data from scheduler
  Stream<List<int>> stream(int id) => schedulers[id].receiveStream;
}

class TcpSocketServer extends GenSocketServer<TcpSocketLibrary> {

  TcpSocketServer(String host, int port,{ bool ssl = false,
    bool split = true, int mtu = PacketScheduler.baseMtu })
      : super(host, port, ssl: ssl, split: split, mtu : mtu) {
    if (_useSecure)
      SecureServerSocket.bind(_host, _port, new SecurityContext())
          .then((srv_sock) {
        srv_sock.listen((sock) => addSock(null, sock)).onError((e,s) {
          logError("Error creating socket server! -> $e \n $s");
        });
      });
    else
      ServerSocket.bind(_host, _port).then((srv_sock) {
        srv_sock.listen((sock) => addSock(sock, null)).onError((e,s) {
          logError("Error creating socket server! $e \n $s");
        });
      });
  }

  void addSock(Socket sock, SecureSocket ssock) {
    int id = ++GenSocketServer.counter;

    instances[id] = new TcpSocketLibrary()
      ..onError = () => removeInstance(id); //Socket initialization

    if (sock != null) instances[id].sock = sock; //Init socket
    if (ssock != null) instances[id].ssock = ssock; //Init secure socket

    schedulers[id] = createObject(instances[id]);

    if (onConnect != null) onConnect(id);
  }
}

class WebSocketServer extends GenSocketServer<WebSocketLibrary> {
  String wsPath = "/ws";
  HttpServer _server;

  //Use static instance
  static WebSocketServer _wss;
  static WebSocketServer get instance {
    if (_wss == null) _wss = new WebSocketServer.dummy(split: true, mtu: 1572864);
    return _wss;
  }

  WebSocketServer(String host, int port, {HttpServer server,
    bool split = true, int mtu = PacketScheduler.baseMtu })
      : super(host, port, ssl : false, split : split, mtu : mtu) {
    _server = server;
    init(); //Create web server or assign from path
  }

  WebSocketServer.check(HttpRequest req,{ bool split = true,
    int mtu = PacketScheduler.baseMtu }) : super (null, null,split:split, mtu:mtu) {
    wsCheck(req); //Run check method directly from another web server
  }

  //Dummy websocket server for static instance
  WebSocketServer.dummy({ bool split = true,
    int mtu = PacketScheduler.baseMtu }) : super(null, null, split:split, mtu:mtu);

  Future<bool> wsCheck(HttpRequest req) {
    Completer<bool> comp = new Completer();
    if (req.uri.path == wsPath)
      WebSocketTransformer.upgrade(req).then((WebSocket ws) {
        int id = ++GenSocketServer.counter;
        //We have websocket ready -> save it and handle it
        ws.pingInterval = const Duration(seconds: 55);

        var wsLib = new WebSocketLibrary()
          ..onError = () => removeInstance(id);
        String ipAddr = req.headers.value("X-Real-IP") ?? req?.connectionInfo?.remoteAddress?.address;
        wsLib.init(ws, ipAddr);

        instances[id] = wsLib;
        schedulers[id] = createObject(instances[id]);
        //print(schedulers[id]);

        if (onConnect != null) onConnect(id);
        comp.complete(true);
      }).catchError((e,s) {
        logError("WSerror: $e \n $s");
        comp.complete(false);
      });

    return comp.future;
  }

  Future<bool> init() async {
    if (_server == null) _server = await HttpServer.bind(_host, _port);
    _server.listen((req) => wsCheck(req));
    return true;
  }
}

class SocketHandler<S extends SocketLibrary, SS extends GenSocketServer<S>> {
  SS _server; //Socket server

  static void set handler(SHandler hnd) => PacketRouter.handler = hnd; //Utility handler
  static SHandler get handler => PacketRouter.handler;

  Map<int, PacketRouter<S>> get schedulers => _server.schedulers;

  SocketHandler(this._server) {
    Message.server = true; //Configure message ids for server
  }

  Future<dynamic> sendMessage(int id, String path, dynamic message) =>
      schedulers[id].sendMessage(message, path);


}





