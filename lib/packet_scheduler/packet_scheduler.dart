part of packet_scheduler;

class PacketScheduler<S extends SocketLibrary> extends SocketLibrary {
  static const int baseMtu = 1480; //Max transfer unit
  String get ipAddr => _socket.ipAddr; //Get ip address from socket
  bool _split;
  int _mtu = baseMtu;

  static List<int> intToBytes(int inp) {
    List<int> bytes = [];
    int size = 0;

    while (inp > 0) {
      bytes.add(inp & 0xff);
      inp = inp >> 8;
      size++;
    }

    //Add integer size will be put in the beginning after reverse
    bytes.add(size);
    bytes = bytes.reversed.toList();
    //print(bytes);

    return bytes;
  }

  static int bytesToInt(List<int> bytes) {
    if (bytes == null || bytes.isEmpty) return -1;

    int res = 0;
    int size = bytes[0];
    int pos = 1;

    while(pos < bytes.length && pos <= size) {
      res = res << 8;
      res = res | bytes[pos];
      pos++;
    }

    //print('size:$size res:$res pos:$pos');

    if (pos+1 < bytes.length) {
      bytes.removeRange(0, pos);
      return res;
    } else
      return -1;
  }

  static createObject<S extends SocketLibrary>(S socket) =>
      new PacketScheduler(socket);

  S _socket;
  List<int> _buffer = []; //Buffer for sending
  List<int> _recvBuffer = []; //Buffer for receiving
  List<int> _tmpBuffer = []; //Temporary receive buffer for not received slices
  int _lastLen = -1; //Last packet length


  PacketScheduler(this._socket,{bool split : true, int mtu : baseMtu }) : super() {
    _split = split;
    _mtu = mtu;

    //Needs a socket to send and receive data
    sendStream.listen((data) {
      if (!_split) _socket.send(data);
        else {
        _buffer.addAll(intToBytes(data.length)); //Add data length
        _buffer.addAll(data); //Add data

        sendSocket(); //Start sending
      }
    });

    _socket.receiveStream.listen((data) {
      if (!_split) addReceived(data);
       else {
        _recvBuffer.addAll(data); //Add received data to buffer
        _receiveData(); //Start data receiving
      }
    });

  }

  void _receiveData() {
    if (_recvBuffer.isEmpty) return; //Exit if nothing to  slice

    //print(_recvBuffer.length);
    int dif;
    if (_lastLen == -1) {
      //Get packet len from receive buffer
      _lastLen = bytesToInt(_recvBuffer);
      if (_lastLen == -1) return; //Packed id sliced on bad place
      dif = _lastLen; //Must not have tmpList all data we need is here
    }
    else dif = _lastLen - _tmpBuffer.length;
    //We've already started packet extracting and need dif more bytes
    //print('dif:$dif');

    if (dif <= _recvBuffer.length) {
      //We have all we need

      List<int> packet;
      packet = _tmpBuffer;
      _tmpBuffer = []; //Remove temporary buffer

      //Remove packet from receive buffer
      if (dif < _recvBuffer.length) {
        packet.addAll(_recvBuffer.sublist(0, dif));
        _recvBuffer = _recvBuffer.sublist(dif);
      }
      else {
        packet.addAll(_recvBuffer); //Add all from buffer
        _recvBuffer = []; //dif == _recvBuffer.length => remove whole buffer
      }

      addReceived(packet);
      //Send resulting packet trough receive controller to be read from client

      _lastLen = -1; //Start a new packet
    } else {
      //dif > _recvBuffer.length -> we don't have the whole packet

      _tmpBuffer.addAll(_recvBuffer);
      //Add all data to _tmpBuffer and continue
      _recvBuffer = [];
      //Remove data from receive buffer
    }

    _receiveData();
  }

  void sendSocket() {
    if (_buffer.isEmpty) return; //Exit on empty  _buffer

    List<int> slice;
    //Not empty so start sending
    if (_buffer.length >= _mtu) {
      slice = _buffer.sublist(0, _mtu); //Slice first mtu bytes
      _buffer = _buffer.sublist(_mtu); //Remove bytes from _buffer
    } else {
      slice = _buffer; //Move _buffer to slice if smaller than mtu
      _buffer = []; //Empty _buffer
    }

    //print('sched: ${slice.length} buf: ${_buffer.length}');
    _socket.send(slice);
    if (_buffer.isNotEmpty) sendSocket(); //If _buffer is not empty send next slice
  }

}


