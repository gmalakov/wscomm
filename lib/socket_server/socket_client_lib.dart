part of socket_server;

class WSClientLib extends SocketLibrary {
  WebSocketLibrary _wsl;

  String _WSPath;

  String get wsPath => _WSPath;

  bool reconnectScheduled = false;
  int _retrySeconds;

  bool get ready => _ready;
  bool _ready = false;

  String _ipAddr;
  String get ipAddr => _ipAddr;

  WSClientLib(String host, int port, { String path: 'ws', bool ssl: false })
      : super() {
    _WSPath = (ssl ? 'wss' : 'ws') + '://$host:$port/$path';
    _ipAddr = host;

    init(); //Init websocket
  }

  init([int retrySeconds = 2]) async {
    reconnectScheduled = false; //Remove scheduled reconnect

    _ready = true;
    WebSocket ws = await WebSocket.connect(wsPath).catchError((err) {
      logError('WS Connect: '+err.toString());
      _ready = false;
    });

    if (!_ready) { //Not connected
      logError("Error openning websocket! $_WSPath");
      _retrySeconds = retrySeconds;
      scheduleReconnect();
    } else {
      _wsl = new WebSocketLibrary();
      _wsl.init(ws); //Init after adding WSClient

      reconnectScheduled = false;
      retrySeconds = 2; //Reset retry seconds on successful connect

      _wsl.receiveStream.listen((data) =>
          addReceived(data)); //Transfer received data to our stream

      sendStream.listen((data) =>
          _wsl.send(data)); //Tranfer our send data to wsl

      _wsl.onError = () {
        logError("WebSocket closed, retrying in $_retrySeconds seconds");
        _ready = false;
        if (onError != null) onError();
        scheduleReconnect();
      };

      if (onConnect != null) onConnect(); //Our OnConnect function
    }
  }

  void scheduleReconnect() {
    //Clear all messages and responses if websocket is disconnected !
    if (!reconnectScheduled) {
      new Timer(
          new Duration(seconds: _retrySeconds), () => init(_retrySeconds * 2));
    }
    reconnectScheduled = true;
  }


}

class SockClientLib extends SocketLibrary {
  TcpSocketLibrary _tcp;

  String _host;
  int _port;

  bool reconnectScheduled = false;
  int _retrySeconds;

  bool _ready = false;
  bool get ready => _ready;

  String get ipAddr => _host;

  SockClientLib(this._host, this._port, { bool ssl: false })
      : super() {
    init(); //Init websocket
  }

  init([int retrySeconds = 2]) async {
    reconnectScheduled = false; //Remove scheduled reconnect

    _tcp = new TcpSocketLibrary();
    bool conn = await _tcp.init(_host, _port); //Init after adding to TCPClient
    _ready = conn; //Set connected or not

    if (!conn) { //Not connected
      logError("Error openning socket! $_host:$_port");
      _retrySeconds = retrySeconds;
      scheduleReconnect();
    } else {
      reconnectScheduled = false;
      retrySeconds = 2; //Reset retry seconds on successful connect

      _tcp.receiveStream.listen((data) =>
          addReceived(data)); //Transfer received data to our stream

      sendStream.listen((data) =>
          _tcp.send(data)); //Tranfer our send data to wsl

      _tcp.onError = () {
        logError("Socket closed, retrying in $_retrySeconds seconds");
        _ready = false;
        if (onError != null) onError();
        scheduleReconnect();
      };

      if (onConnect != null) onConnect(); //Our OnConnect function
    }
  }

  void scheduleReconnect() {
    //Clear all messages and responses if websocket is disconnected !
    if (!reconnectScheduled) {
      new Timer(
          new Duration(seconds: _retrySeconds), () => init(_retrySeconds * 2));
    }
    reconnectScheduled = true;
  }


}
