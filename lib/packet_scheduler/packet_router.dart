part of packet_scheduler;

class PacketRouter<S extends SocketLibrary> extends PacketScheduler<S> {
  static SHandler handler;

  MsgCache _sentMessages = new MsgCache(); //Sent messages
  String sesId; //Session id
  PacketRouter(S socket,{bool split : true, int mtu : PacketScheduler.baseMtu })
      : super(socket,split:split, mtu:mtu) {
    receiveStream.listen((dynamic message) => _handleMessage(message));
  }

  void _handleMessage(dynamic message) {
    Message msg = new Message.fromBin(message as List<int>);
    //print(msg); //Print received message

    if (msg.pth == "auth") {
      sesId = msg.msg["sesid"] as String;
    }
    else {
      Message cMsg = _sentMessages.findMsg(msg);
      if (cMsg != null) {
        //It's a response
        //It's a response
        //print('returned $cMsg');
        if (cMsg.handler != null) cMsg.handler(msg.msg); //This is answer for the current message
        _sentMessages.remove(cMsg.id); //Remove finished element
      } else {
        //print('received $msg');
        if (msg.id != null)
           _handleRequest(msg); //when is not a response
      }
    }
  }

  //Will be used on client for sending authentication
  void sendAuth() {
    if ((sesId ?? "").length > 0)
      sendMessageCB({"sesid": sesId}, "auth", null, response: true); //Send as response -> not added to sentMessages
  }


  void sendMessageCB(dynamic message, String path, CallBackFunc callBack, {int id, bool response:false}) {
    Message msg = new Message(message, path, callBack, id:id);
    //print("id:${msg.id} path:${msg.pth} data:${msg.msg}");
    send(msg.toBin()); //Send packet to scheduler
    //If it's not a response then add it to cache
    if (!response) _sentMessages.setMsg(msg);
  }

  Future<dynamic> sendMessage(dynamic message, String path, {int id, bool response: false}) {
    Completer<dynamic> comp = new Completer();
    sendMessageCB(message, path, (dynamic res) => comp.complete(res), id:id, response: response);
    return comp.future;
  }

  void _handleRequest(Message msg) {
    //logData(msg.toString());
    Message cMsg = _sentMessages.findMsg(msg);
    if (cMsg != null) {
      //This is a message response
      if (cMsg?.handler != null) cMsg.handler(msg); //This is answer for the current message
      _sentMessages.remove(cMsg.id); //Remove message from sent map
      //readySnd = true; //Ready to send Yes
    } else {
      //This is a new message from client
      /* execCommand(String cmd, Map pData, Function callBack) */
      if (msg.msg is! Map) msg.msg = <String, dynamic>{"res": msg.msg}; //Put into map if String value
      Map pData;
      try { pData = msg.msg as Map; } catch (err) { logError("Error: $msg ||| $err"); }

      if (pData == null) {
        logError("Malformed data: command = ${msg.pth} id = ${msg.id} msg=${msg.msg}");
        sendMessageCB("Malformed data!", msg.pth, null, id: msg.id, response: true);
        return;
      }

      //Set login parameters to execution map
      pData["login"] = sesId;
      pData["ip"] = ipAddr;
      //print (pData.toString()); //Print map to be passed to handler

      //print('Executing $pData -> $handler');
      if (handler != null) handler(msg.pth, pData, ((dynamic res) => sendMessageCB(res, msg.pth, null, id: msg.id, response:true)));
    }
  }

}
