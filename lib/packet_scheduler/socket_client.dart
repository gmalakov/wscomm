part of packet_scheduler;

class SocketClient<S extends SocketLibrary> {
  static void set handler(SHandler hnd) =>
      PacketRouter.handler = hnd; //Utility handler
  static SHandler get handler => PacketRouter.handler;

  S _socket;
  PacketRouter<S> _router;

  bool _ready = false;

  SimpleCallBackFunc onReady;
  void set onError(SimpleCallBackFunc err) => _socket.onError = err;
  SimpleCallBackFunc get onError => _socket.onError;

  //Create websocket client
  SocketClient(this._socket, {bool split : true, int mtu : PacketScheduler.baseMtu }) {
    //Create packet router
    _router = new PacketRouter(_socket, split:split, mtu:mtu);

    _socket.onConnect = () {
      _router.sendAuth(); //Send authentication on connect
      _ready = true; //Set ready flag
      if (onReady != null) onReady();
    };
  }

  void set sesId(String sid) {
    if (sid == _router.sesId) return;
    //No action needed when not chaning session id
    _router.sesId = sid;
    _router.sendAuth(); //Send authentication on set sesId
  }

  String get sesId => _router.sesId;

  Future<bool> _waitReady() {
    //Already in game
    if (_ready) return new Future.value(true);
    Completer<bool> comp = new Completer();

    Function onC = _socket.onConnect;
    _socket.onConnect = () {
      if (onC != null) onC;
      _socket.onConnect = onC; //Return old onConnect command
      _ready = true;
      comp.complete(true); //OnConnect return greenlight for sendMessage
    };

    return comp.future;
  }

  Future<dynamic> sendMessage(dynamic data, String path) async {
    await _waitReady();
    return _router.sendMessage(data, path);
  }

  void sendMessageCB(dynamic data, String path, CallBackFunc callBack) {
    _waitReady().then((_) => _router.sendMessageCB(data, path, callBack));
  }

}
