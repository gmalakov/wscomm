part of socket_server;

class WSClient extends SocketClient<WSClientLib> {
  static void set handler(SHandler hnd) =>
      PacketRouter.handler = hnd; //Utility handler
  static SHandler get handler => PacketRouter.handler;

  WSClient(String host, int port, { String path : 'ws', bool ssl : false,
    bool split : true, int mtu : PacketScheduler.baseMtu }) :
        super(new WSClientLib(host, port, path:path, ssl:ssl),
          split:split, mtu:mtu) {
            handler = MessageHandler.execCommand;
  }
}

class TCPClient extends SocketClient<SockClientLib> {
  static void set handler(SHandler hnd) =>
      PacketRouter.handler = hnd; //Utility handler
  static SHandler get handler => PacketRouter.handler;

  TCPClient(String host, int port, { bool ssl : false,
    bool split : true, int mtu : PacketScheduler.baseMtu }) : super(
    new SockClientLib(host, port, ssl: ssl), split:split, mtu: mtu) {
     handler = MessageHandler.execCommand;
  }
}