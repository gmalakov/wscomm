library sock_client;

import 'dart:html';
import 'dart:async';
import 'shared.dart';
import 'dart:typed_data';

import 'packet_scheduler.dart';

class WSLibrary extends SocketLibrary {
  WebSocket _ws;

  bool get ready => (_ws?.readyState == WebSocket.OPEN); //is websocket ready
  String _WSPath;

  bool reconnectScheduled = false;
  int _retrySeconds;

  WSLibrary(this._WSPath) : super();

  //Close websocket
  close() => _ws.close();

  String _ipAddr = "";

  String get ipAddr => _ipAddr;

  //Add actual websocket
  init([int retrySeconds = 2]) {
    reconnectScheduled = false; //Remove scheduled reconnect
    _ws = new WebSocket(_WSPath)..binaryType = 'arraybuffer';
    if (_ws == null) {
      logError("Error openning websocket! $_WSPath");
      scheduleReconnect();
    } else {
      _retrySeconds = retrySeconds;

      _ws?.onOpen?.listen((e) {
        reconnectScheduled = false; //Disable automtic recconnect on opened port
        _retrySeconds = 2; //Reset retry seconds
        if (onConnect != null) onConnect();
      });

      _ws?.onClose?.listen((e) {
        logError("WebSocket closed, retrying in $_retrySeconds seconds");
        if (onError != null) onError();
        scheduleReconnect();
      });

      _ws?.onError?.listen((e) {
        logError("Error connecting to WebSocket");
        if (onError != null) onError();
        scheduleReconnect();
      });

      _ws?.onMessage?.listen((MessageEvent e) {
        try {
          var tmp = e.data.asUint8List();
          addReceived(tmp);
        } catch (err) {
          logError('Recieved ${e.data} form ws!');
        }
      })?.onError((e, s) {
        logError('Error $e \n $s');
        if (onError != null) onError();
      });
    }

    if (!sendStreamHasListener())
      sendStream.listen((data) {
        //print('S:$data');
        if (ready)
          _ws.send(new Uint8List.fromList(data));
        else if (onError != null) onError(); //Websocket is not connected
      }).onError((err) {
        logError(err);
        if (onError != null) onError();
      }); //Add received data to websocket
  }

  void scheduleReconnect() {
    //Clear all messages and responses if websocket is disconnected !

    if (!reconnectScheduled) {
      new Timer(
          new Duration(seconds: _retrySeconds), () => init(_retrySeconds * 2));
    }
    reconnectScheduled = true;
  }
}

class WSClient extends SocketClient<WSLibrary> {
  static void set handler(SHandler hnd) =>
      PacketRouter.handler = hnd; //Utility handler
  static SHandler get handler => PacketRouter.handler;

  //1.5MB packets
  WSClient(String wsPath) : super(new WSLibrary(wsPath)..init(),
      split:true, mtu: 1572864) {
    handler = MessageHandler.execCommand;
  }
}
