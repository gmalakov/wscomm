part of shared;

typedef void CallBackFunc(dynamic res);
typedef void HandFunc(dynamic data, CallBackFunc callBack);
typedef void SHandler(String path, Map pData, CallBackFunc callBack);

class MessageHandler {
  //Function (data, callback)
  static Map<String, HandFunc> handMap = new Map();

  static void execCommand(String path, dynamic data, CallBackFunc callBack) {
    if (handMap[path] != null) handMap[path](data, callBack);
     else callBack("ERROR! Function not available!"); //Respond with function not found
  }
}