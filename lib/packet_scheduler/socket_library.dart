part of packet_scheduler;

Function logError = print;
Function logData = print;


class SocketLibrary {

  SimpleCallBackFunc onError;
  SimpleCallBackFunc onConnect;

  StreamController<List<int>> _receiveContr = new StreamController();
  StreamController<List<int>> _sendContr = new StreamController();

  Stream<List<int>> get receiveStream => _receiveContr.stream;
  Stream<List<int>> get sendStream => _sendContr.stream;

  SocketLibrary();
  void close() => _receiveContr.close(); //Close stream

  bool sendStreamHasListener() => _sendContr.hasListener;
  void send(List<int> bytes) => _sendContr.add(bytes); //Send data trough socket
  void addReceived(List<int> bytes) => _receiveContr.add(bytes); //Receive data

  String get ipAddr => "";
}
